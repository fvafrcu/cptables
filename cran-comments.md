Dear CRAN Team,
this is a resubmission of package 'cptables'. I have added the following changes:

* Fixed typo in visualization.

Please upload to CRAN.
Best, Andreas Dominik

# Package cptables 0.3.1
## Test  environments 
- R Under development (unstable) (2018-07-01 r74949)
    Platform: x86_64-pc-linux-gnu (64-bit)
    Running under: Debian GNU/Linux 9 (stretch)
    0 errors | 1 warning  | 1 note 
- win-builder (devel)
