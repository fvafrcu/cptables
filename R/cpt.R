#' Read a Conditional Probability Table
#'
#' Read a Conditional Probability Table (probably written via
#' \code{\link{write_cpt}} into an
#' \code{\link[gRain:cptable]{gRain::cptable}}.
#' @param path The path to the file containing the cpt.
#' @return A \code{\link[gRain:cptable]{gRain::cptable}}.
#' @export
#' @examples
#' node_a <- gRain::cptable(~node_a, levels = c(0, 1), values = c(0.97, 0.01))
#' node_b <- gRain::cptable(~node_b, levels = c("A", "B", "C"),
#'                          values = c(0.98, 0.01, 0.01))
#' node_c <- gRain::cptable(~node_c, levels = c(TRUE, FALSE),
#'                          values = c(0.97, 0.01))
#' node_v <- gRain::cptable(~node_v | node_a + node_b + node_c,
#'                          levels = c("JA", "NEIN"),
#'                        values = c(0.01, 0.99, 0.02, 0.98, 0.03, 0.97, 0.04,
#'                                   0.96, 0.05, 0.95, 0.06, 0.94, 0.07, 0.93,
#'                                   0.08, 0.92, 0.09, 0.91, 0.10, 0.90, 0.11,
#'                                   0.89, 0.12, 0.88))
#' temporary_file <- tempfile()
#' write_cpt(node_v, temporary_file)
#' tmp <- read_cpt(path = temporary_file)
#' identical(node_v, tmp)
read_cpt <- function(path) {
   lines <- readLines(path)
   data <- sub(" ?\\|.*", "", lines[grep("^#.*$", lines, invert = TRUE)])
   pattern  <- "^#\\s?vpar:\\s?"
   vpar <- trimws(unlist(strsplit(sub(pattern, "",
                                      grep(paste0(pattern, ".*$"), lines,
                                           value = TRUE)), split = ",")))
   values <- as.vector(t(utils::read.table(header = FALSE, text = data,
                                           sep = "")))
   pattern  <- "^#\\s?levels:\\s?"
   levels <- trimws(unlist(strsplit(sub(pattern, "",
                                        grep(paste0(pattern, ".*$"), lines,
                                             value = TRUE)), split = ",")))
   if (!all(is.na(suppressWarnings(as.numeric(levels))))) {
       levels <- as.numeric(levels)
   } else {
       ## after as.numeric, as.logical always works and vice versa, so _never_
       ## do both.
       if (!all(is.na(as.logical(levels)))) levels <- as.logical(levels)
   }
   cpt <- gRain::cptable(vpar, levels = levels, values = values,
                         normalize = TRUE)
   return(cpt)
}

#' Write a Conditional Probability Table
#'
#' Write a commented \code{\link[gRain:cptable]{gRain::cptable}}.
#' @param cpt The conditional probability table to write.
#' @param path The path to write to, defaults to console.
#' @param parent_node_env The environment to search parental nodes in. For
#' internal use only, stick with the default.
#' @return \code{\link[base:invisible]{Invisibly} the path specified. But is
#' called for its side effect of writing the table.
#' @export
#' @examples
#' node_a <- gRain::cptable(~node_a, levels = c(0, 1), values = c(0.97, 0.01))
#' node_b <- gRain::cptable(~node_b, levels = c("A", "B", "C"),
#'                          values = c(0.98, 0.01, 0.01))
#' node_c <- gRain::cptable(~cnode_, levels = c(TRUE, FALSE),
#'                          values = c(0.97, 0.01))
#' node <- gRain::cptable(~v | node_a + node_b + node_c,
#'                        levels = c("YES", "NO"),
#'                        values = c(0.01, 0.99, 0.02, 0.98, 0.03, 0.97, 0.04,
#'                                   0.96, 0.05, 0.95, 0.06, 0.94, 0.07, 0.93,
#'                                   0.08, 0.92, 0.09, 0.91, 0.10, 0.90, 0.11,
#'                                   0.89, 0.12, 0.88))
#' write_cpt(node)
#' rm(node_a)
#' # No comments are written, since the cptable ist conditioned on a, which
#' # needs an object \code{node_a}.
#' suppressWarnings(write_cpt(node))
write_cpt <- function(cpt, path = "", parent_node_env = pos.to.env(-1L)) {
    vpar <- paste("# vpar:", paste(cpt[["vpa"]], collapse = ", "))
    levels <- paste("# levels:", paste(cpt[["levels"]], collapse = ", "))
    values <- matrix(cpt[["values"]],
                     ncol = length(cpt[["levels"]]), byrow = TRUE)

    n_parents <- length(cpt[["vpa"]]) - 1
    if (as.logical(n_parents)) {
        exist <- TRUE
        n_parent_node_levels <- NULL
        parents <- NULL
        for (i in seq.int(from = 2, length.out = n_parents)) {
            parent <- cpt[["vpa"]][i]
            parent_name <- parent
            parent_exists <- exists(parent_name, envir = parent_node_env)
            if (parent_exists) {
                parent_node <- get(parent_name, , envir = parent_node_env)
                if (methods::is(parent_node, "cptable")) {
                    n_parent_node_levels <- c(n_parent_node_levels,
                                      length(parent_node[["levels"]]))
                } else {
                    parent_exists <- FALSE
                }
                parents <- c(parents, parent)
                names(n_parent_node_levels) <- parents
            } else {
                warning(parent_name, " not found in ",
                        paste(search(), collpase = " "), ".")
            }
            exist <- c(exist, parent_exists)
        }
    }
    if (exists("exist") && all(exist)) {
        parent_values <- NULL
        for (i in seq.int(from = 2, length.out = n_parents)) {
            parent <- cpt[["vpa"]][i]
            parent_name <- parent
            parent_node <- get(parent_name, envir = parent_node_env)
            current_parental_index <- i - 1
            before <- n_parent_node_levels[seq(1, current_parental_index - 1)]
            after <- n_parent_node_levels[seq(current_parental_index + 1,
                                              n_parents)]
            # exclude current position, set to 1 for Reduce as 1 is neutral to
            # times and each argument of rep()
            if (current_parental_index == 1) {
                before = 1
            }
            if (current_parental_index == n_parents) {
                after = 1
            }
            parent_values <- cbind(parent_values,
                                   rep(parent_node[["levels"]],
                                       times = Reduce("*", after),
                                       each = Reduce("*", before)))
        }
        values <- cbind(values, "|", parent_values)
        names <-  c(cpt[["levels"]], "|", parents)
        names[1] <- paste0("# ", names[1])
        colnames(values) <- names
    } else {
            names <-  c(cpt[["levels"]])
            names[1] <- paste0("# ", names[1])
            colnames(values) <- names
    }
    cat(vpar, file = path, append = FALSE, sep = "\n")
    cat(levels, file = path, append = TRUE, sep = "\n")
    cat("# conditional probability values:", file = path, append = TRUE,
        sep = "\n")
    digits <- max(nchar(as.character(sub("[0-9]*\\.", "", values))))
    suppressWarnings(utils::write.table(format(values, digits = digits),
                                        file = path, row.names = FALSE,
                                        col.names = TRUE, append = TRUE, # those
                                        # two TRUE issues a warning, so we
                                        # suppress _all_ warninigs...
                                 sep = " ", quote = FALSE))
    return(invisible(path))
}
