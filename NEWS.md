# cptables 0.3.1.9000

# cptables 0.3.1

* Fixed typo in visualization.

# cptables 0.3.0

* Added visualization for files containing conditional probability tables.

# cptables 0.2.0

* Nodes of the baysian belief networks (and their conditional probability tables
  do not have to be named name\_*XXX* anymore.

# cptables 0.1.0

* Added a `NEWS.md` file to track changes to the package.



