if (interactive()) pkgload::load_all()
test_extract <- function() {
    bbn <- read_grain()
    RUnit::checkException(extract_cptable(bbn, "foo"))
    assign("result", extract_cptable(bbn, "storm_phenips"))
    expectation <- read_cpt(system.file("files", "node_storm_phenips.cpt",
                                               package ="cptables"))
    RUnit::checkIdentical(result, expectation)
}

test_read_write <- function() {
    bbn <- read_grain()
    out_dir <- file.path(tempdir(), "bbn")
    write_grain(grain = bbn, path = out_dir)
    
    result <- readLines(file.path(out_dir, "node_vulnerability.cpt"))
    if (FALSE) {
        write_grain(grain = bbn, path = "./inst/files/")
    }
    expectation <- readLines(system.file("files", "node_vulnerability.cpt",
                                               package ="cptables"))

    RUnit::checkIdentical(result, expectation)
}
