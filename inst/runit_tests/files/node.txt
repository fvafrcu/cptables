# vpar: node_v, node_a, node_b, node_c
# levels: YES, NO
# conditional probability values:
# YES NO
0.01 0.99
0.02 0.98
0.03 0.97
0.04 0.96
0.05 0.95
0.06 0.94
0.07 0.93
0.08 0.92
0.09 0.91
0.10 0.90
0.11 0.89
0.12 0.88
