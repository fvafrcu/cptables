require("gRain")
node.whh <- cptable(~whh
, levels=c (0, 1, 2, 3)
, values =c( 0.97, 0.01, 0.01, 0.01)
)
node.kg <- cptable(~kg
, levels=c (1, 2, 4, 6, 10, 30)
, values =c( 0.95, 0.01, 0.01, 0.01, 0.01, 0.01)
)
node.whh_kg <- cptable(~whh_kg | kg+whh
, levels=c (TRUE, FALSE)
, values =c( 0, 1,  0.01, 0.99,  0.05, 0.95,  0.1, 0.9,  0.15, 0.85,  0.2, 0.8,  0.01, 0.99,  0.05, 0.95,  0.1, 0.9,  0.15, 0.85,  0.2, 0.8,  0.25, 0.75,  0.05, 0.95,  0.1, 0.9,  0.2, 0.8,  0.25, 0.75,  0.3, 0.7,  0.35, 0.65,  0.1, 0.9,  0.15, 0.85,  0.25, 0.75,  0.3, 0.7,  0.35, 0.65,  0.4, 0.6 )
)
node.sdm <- cptable(~sdm
, levels=c (1, 2, 3, 4)
, values =c( 0.97, 0.01, 0.01, 0.01)
)
node.sdm_whh <- cptable(~sdm_whh | whh+sdm
, levels=c ("GUT","GEEIGNET", "UNGEEIGNET", "SCHLECHT")
, values =c( 1, 0, 0, 0,  0.45, 0.45, 0.1, 0,  0.25, 0.5, 0.25, 0,  0.1, 0.4, 0.4, 0.1,  0.45, 0.45, 0.1, 0,  0.05, 0.9, 0.05, 0,  0.05, 0.45, 0.45, 0.05,  0.1, 0.25, 0.4, 0.25,  0.25, 0.4, 0.25, 0.1,  0.05, 0.45, 0.45, 0.05,  0, 0.05, 0.9, 0.05,  0, 0.1, 0.45, 0.45,  0.1, 0.4, 0.4, 0.1,  0, 0.25, 0.5, 0.25,  0, 0.1, 0.45, 0.45,  0, 0, 0.1, 0.9 )
)
node.sr <- cptable(~sr
, levels=c (10, 50, 90, 100)
, values =c( 0.97, 0.01, 0.01, 0.01)
)
node.sr_kg <- cptable(~sr_kg | kg+sr
, levels=c (TRUE, FALSE)
, values =c( 0, 1,  0.01, 0.99,  0.05, 0.95,  0.1, 0.9,  0.15, 0.85,  0.2, 0.8,  0.01, 0.99,  0.05, 0.95,  0.1, 0.9,  0.15, 0.85,  0.2, 0.8,  0.25, 0.75,  0.05, 0.95,  0.1, 0.9,  0.2, 0.8,  0.25, 0.75,  0.3, 0.7,  0.35, 0.65,  0.1, 0.9,  0.15, 0.85,  0.25, 0.75,  0.3, 0.7,  0.35, 0.65,  0.4, 0.6 )
)
node.kb <- cptable(~kb | whh_kg+sr_kg
, levels=c (TRUE, FALSE)
, values =c( 1,0,  1,0,  1,0,  0,1 )
)
node.vulnerabilitaetsindex <- cptable(~vulnerabilitaetsindex | sr+kb+sdm_whh
, levels=c ("MAXIMAL", "HOCH", "GERING", "MINIMAL")
, values =c( 0,0,1,0,  0,0,1,0,  0,1,0,0,  0,1,0,0,  0,0,0,1,  0,0,0,1,  0,0,1,0,  0,0,1,0,  0,1,0,0,  0,1,0,0,  0,1,0,0,  1,0,0,0,  0,0,0,1,  0,0,1,0,  0,0,1,0,  0,1,0,0,  0,1,0,0,  0,1,0,0,  1,0,0,0,  1,0,0,0,  0,0,0,1,  0,0,1,0,  0,1,0,0,  1,0,0,0,  1,0,0,0,  1,0,0,0,  1,0,0,0,  1,0,0,0,  0,0,1,0,  0,1,0,0,  0,1,0,0,  1,0,0,0 )
)
plist <- compileCPT(list(node.whh,node.kg,node.whh_kg,node.sdm,node.sdm_whh,node.sr,node.sr_kg,node.kb,node.vulnerabilitaetsindex)) 
grainedlist <- grain(plist)
### querygrain(grainedlist,nodes=c("sdm_whh"), type="marginal")
### querygrain(grainedlist,nodes=c("vulnerabilitaetsindex"), type="marginal")
