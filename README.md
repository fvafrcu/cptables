---
output: github_document
---
[![pipeline status](https://gitlab.com/fvafrcu/cptables/badges/master/pipeline.svg)](https://gitlab.com/fvafrcu/cptables/commits/master)    
[![coverage report](https://gitlab.com/fvafrcu/cptables/badges/master/coverage.svg)](https://gitlab.com/fvafrcu/cptables/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrcu/cptables.svg?branch=master)](https://travis-ci.org/fvafrcu/cptables)
    [![Coverage Status](https://codecov.io/github/fvafrcu/cptables/coverage.svg?branch=master)](https://codecov.io/github/fvafrcu/cptables?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/cptables)](https://cran.r-project.org/package=cptables)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/cptables)](https://cran.r-project.org/package=cptables)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/cptables)](https://cran.r-project.org/package=cptables)

<!-- README.md is generated from README.Rmd. Please edit that file -->



# cptables
## Introduction
Please read the
[vignette](https://fvafrcu.gitlab.io/cptables/inst/doc/An_Introduction_to_cptables.html).

Or, after installation, the help page:

```r
help("cptables-package", package = "cptables")
```

```
#> Write and Read Conditional Probability Tables
#> 
#> Description:
#> 
#>      Write and read conditional probability tables for baysian belief
#>      networks implemented in package gRain.
#> 
#> Details:
#> 
#>      You will find the details in
#>      'vignette("An_Introduction_to_cptables", package = "cptables")'.
```

## Installation

You can install cptables from github with:


```r
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("fvafrcu/cptables")
```


